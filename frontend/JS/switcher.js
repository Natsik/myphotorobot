/*function MySwitcher(idDealObj,idWorkersObj){
    for(var i = 0; i < idWorkersObj.length; i++){
        $("#"+idWorkersObj[i]).click(idDealObj,function(){
            var thisObjWidth = $(this).outerWidth();
            var leftDeviation = $(this).position();
            $("#"+idDealObj).animate({'width':thisObjWidth,'left':leftDeviation.left},300);
        });
    }
}*/
function SetMySwitcher(idDealObj,idWorkerObj){
    var WorkerObj = $("#"+idWorkerObj);
    var thisObjWidth = WorkerObj.outerWidth();
    var leftDeviation = WorkerObj.position();
    $("#"+idDealObj).animate({'width':thisObjWidth,'left':leftDeviation.left},600);
}

function MySwitcher(idDealObj,idWorkersObj){
    for(var i = 0; i < idWorkersObj.length; i++){
        $("#"+idWorkersObj[i]).change(idDealObj,function(){
            var oldId = $(this).attr('id');
            var newId = oldId.substring(0,oldId.length-6);
            var thisObjWidth = $("#"+newId).outerWidth();
            var leftDeviation = $("#"+newId).position();
            $("#"+idDealObj).animate({'width':thisObjWidth,'left':leftDeviation.left},300);
        });
    }
}