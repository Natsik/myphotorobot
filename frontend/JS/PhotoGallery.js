var Gallery = {
    photos : [
        {
            src:"img/testPhotos/1.png",
            name:"Marry ME",
            gender:"f",
            subscribers:[
                {
                    name:"Kate Wong",
                    gender:"f",
                    src:""
                },
                {
                    name:"Vera 22",
                    gender:"f",
                    src:""
                },
                {
                    name:"Max 221",
                    gender:"m",
                    src:""
                },
                {
                    name:"Криси",
                    gender:"f",
                    src:""
                }
            ],
            town:"Санкт-Петербург",
            point:"Студия Луч",
            time:"13:40",
            srcCheckIn:"",
            srcPickForMe:"",
            countComments:2, srcComment:""
        },
        {
            src:"img/testPhotos/1.png",
            name:"Marry ME",
            gender:"f",
            subscribers:[
                {
                    name:"Kate Wong",
                    gender:"f",
                    src:""
                },
                {
                    name:"Vera 22",
                    gender:"f",
                    src:""
                },
                {
                    name:"Max 221",
                    gender:"m",
                    src:""
                },
                {
                    name:"Криси",
                    gender:"f",
                    src:""
                }
            ],
            town:"Санкт-Петербург",
            point:"Студия Луч",
            time:"13:40",
            srcCheckIn:"",
            srcPickForMe:"",
            countComments:2, srcComment:""
        },
        {
            src:"img/testPhotos/1.png",
            name:"Marry ME",
            gender:"f",
            subscribers:[
                {
                    name:"Kate Wong",
                    gender:"f",
                    src:""
                },
                {
                    name:"Vera 22",
                    gender:"f",
                    src:""
                },
                {
                    name:"Max 221",
                    gender:"m",
                    src:""
                },
                {
                    name:"Криси",
                    gender:"f",
                    src:""
                }
            ],
            town:"Санкт-Петербург",
            point:"Студия Луч",
            time:"13:40",
            srcCheckIn:"",
            srcPickForMe:"",
            countComments:2, srcComment:""
        },
        {
            src:"img/testPhotos/1.png",
            name:"Marry ME",
            gender:"f",
            subscribers:[
                {
                    name:"Kate Wong",
                    gender:"f",
                    src:""
                },
                {
                    name:"Vera 22",
                    gender:"f",
                    src:""
                },
                {
                    name:"Max 221",
                    gender:"m",
                    src:""
                },
                {
                    name:"Криси",
                    gender:"f",
                    src:""
                }
            ],
            town:"Санкт-Петербург",
            point:"Студия Луч",
            time:"13:40",
            srcCheckIn:"",
            srcPickForMe:"",
            countComments:2, srcComment:""
        },
        {
            src:"img/testPhotos/1.png",
            name:"Marry ME",
            gender:"f",
            subscribers:[
                {
                    name:"Kate Wong",
                    gender:"f",
                    src:""
                },
                {
                    name:"Vera 22",
                    gender:"f",
                    src:""
                },
                {
                    name:"Max 221",
                    gender:"m",
                    src:""
                },
                {
                    name:"Криси",
                    gender:"f",
                    src:""
                }
            ],
            town:"Санкт-Петербург",
            point:"Студия Луч",
            time:"13:40",
            srcCheckIn:"",
            srcPickForMe:"",
            countComments:2, srcComment:""
        },
        {
            src:"img/testPhotos/3.png",
            name:"Marry ME",
            gender:"m",
            subscribers:[
                {
                    name:"Kate Wong",
                    gender:"f",
                    src:""
                },
                {
                    name:"Vera 22",
                    gender:"f",
                    src:""
                },
                {
                    name:"Max 221",
                    gender:"m",
                    src:""
                },
                {
                    name:"Криси",
                    gender:"f",
                    src:""
                }
            ],
            town:"Санкт-Петербург",
            point:"Студия Луч",
            time:"13:40",
            srcCheckIn:"",
            srcPickForMe:"",
            countComments:2, srcComment:""
        },
        {
            src:"img/testPhotos/1.png",
            name:"Marry ME",
            gender:"f",
            subscribers:[
                {
                    name:"Kate Wong",
                    gender:"f",
                    src:""
                },
                {
                    name:"Vera 22",
                    gender:"f",
                    src:""
                },
                {
                    name:"Max 221",
                    gender:"m",
                    src:""
                },
                {
                    name:"Криси",
                    gender:"f",
                    src:""
                }
            ],
            town:"Санкт-Петербург",
            point:"Студия Луч",
            time:"13:40",
            srcCheckIn:"",
            srcPickForMe:"",
            countComments:2, srcComment:""
        },
        {
            src:"img/testPhotos/2.png",
            name:"Marry ME",
            gender:"f",
            subscribers:[
                {
                    name:"Kate Wong",
                    gender:"f",
                    src:""
                },
                {
                    name:"Vera 22",
                    gender:"f",
                    src:""
                },
                {
                    name:"Max 221",
                    gender:"m",
                    src:""
                },
                {
                    name:"Криси",
                    gender:"f",
                    src:""
                }
            ],
            town:"Санкт-Петербург",
            point:"Студия Луч",
            time:"13:40",
            srcCheckIn:"",
            srcPickForMe:"",
            countComments:2, srcComment:""
        },
        {
            src:"img/testPhotos/2.png",
            name:"Marry ME",
            gender:"m",
            subscribers:[
                {
                    name:"Kate Wong",
                    gender:"f",
                    src:""
                },
                {
                    name:"Vera 22",
                    gender:"f",
                    src:""
                },
                {
                    name:"Max 221",
                    gender:"m",
                    src:""
                },
                {
                    name:"Криси",
                    gender:"f",
                    src:""
                }
            ],
            town:"Санкт-Петербург",
            point:"Студия Луч",
            time:"13:40",
            srcCheckIn:"",
            srcPickForMe:"",
            countComments:2, srcComment:""
        }
    ],
    pageNum : 2,
    maxPage : 100
};

function photoD(photoWrapperId,numberWrapperId){
    var html = "";
    for(var i = 0; i < Gallery.photos.length; i++){
        html += "<div id='photoNum_"+i+"' class='photo-wrapper' onclick='photoClicked(this);'>";
        html += "   <img width='122' height='117' src='"+Gallery.photos[i].src+"'>";
        var textColor = (Gallery.photos[i].gender == 'f') ? "rgb(204,0,0)":"rgb(51,51,51)";
        html += "   <div class='photo-text' style='color:"+textColor+";'>";
        html += Gallery.photos[i].name;
        html += "   </div>";
        html += "</div>";
        if((((i+1) % 8) == 0) || i == (Gallery.photos.length - 1)){
            html += "<div class='clear'></div>";
        }
    }

    $(photoWrapperId).html(html);

    html = "<table><tr>";
    for(var i = 1; i < Gallery.maxPage; i++){
        var numClass = "page-number-button" + ((i == Gallery.pageNum) ? " now":"");
        html += "<td><div class='"+numClass+"'>";
        html += i;
        html += "</div></td>";
    }
    html += "</tr></table>";
    $(numberWrapperId).html(html);
}

function photoClicked(obj){
    var jObj = $(obj);
    if(jObj.hasClass("checked")){
        unClickedPhotos();
        return 0;
    }

    unClickedPhotos();

    var index = jObj.attr('id').split("_")[1];

    jObj.addClass("checked");
    jObj.append("<div class='cover-photo'></div>");
    jObj.children(".photo-text").css('visibility','hidden');

    var ofset = jObj.offset();

    var html = "";
    html += "<div id='photo-more-info-wrapper'>";
    html += "<div id='photo-more-info-background'>";
    html += "   <div class='photo-more-info-leftColumn'>";
    for(var i = 0; i < Gallery.photos[index].subscribers.length; i++) {
        if (i % 2 == 0) {
            var postHtmlA = "";
            if (Gallery.photos[index].subscribers[i].src) {
                html += "<a href='" + Gallery.photos[index].subscribers[i].src + "'>";
                postHtmlA = "</a>"
            }
            var genderColor = (Gallery.photos[index].subscribers[i].gender == "f") ? "rgb(204,0,0)" : "rgb(51,51,51)";
            html += "<span style='color:"+genderColor+"'>";
            html += Gallery.photos[index].subscribers[i].name;
            html += "</span>";
            html += postHtmlA;
            html += "<br />"
        }
    }
    html += "   </div>";
    html += "   <div class='photo-more-info-rightColumn'>";
    for(var i = 0; i < Gallery.photos[index].subscribers.length; i++) {
        if (i % 2 == 1) {
            var postHtmlA = "";
            if (Gallery.photos[index].subscribers[i].src) {
                html += "<a href='" + Gallery.photos[index].subscribers[i].src + "'>";
                postHtmlA = "</a>"
            }
            var genderColor = (Gallery.photos[index].subscribers[i].gender == "f") ? "rgb(204,0,0)" : "rgb(51,51,51)";
            html += "<span style='color:"+genderColor+"'>";
            html += Gallery.photos[index].subscribers[i].name;
            html += "</span>";
            html += postHtmlA;
            html += "<br />"
        }
    }
    html += "   </div>";
    html += "   <div class='clear'>";
    html += "   </div>";
    html += "   <div class='horizontal-line'>";
    html += "   </div>";
    html += "   <div id='photo-more-info-infoPoint'>";
    if(Gallery.photos[index].town){
        html += Gallery.photos[index].town + "<br />";
    }
    if(Gallery.photos[index].point){
        html += Gallery.photos[index].point + "<br />";
    }
    if(Gallery.photos[index].time){
        html += Gallery.photos[index].time + "<br />";
    }
    html += "   </div>";
    html += "   <div class='horizontal-line'>";
    html += "   </div>";
    html += "   <div class='photo-more-info-leftColumn'>";
    html += "       <a href='"+Gallery.photos[index].srcCheckIn+"' style='color:rgb(204,0,0)'>Отметиться</a>";
    html += "   </div>";
    html += "   <div class='photo-more-info-rightColumn'>";
    html += "       <a href='"+Gallery.photos[index].srcPickForMe+"' style='color:rgb(204,0,0)'>Забрать к себе</a>";
    html += "   </div>";
    html += "   <div class='clear'>";
    html += "   </div>";
    html += "   <div class='horizontal-line'>";
    html += "   </div>";
    html += "   <div class='photo-more-info-leftColumn'>";
    html += "       <a href='"+Gallery.photos[index].srcComment+"' style='color:rgb(153,153,153)'>Комментировать</a>";
    html += "   </div>";
    html += "   <div class='photo-more-info-rightColumn'>";
    html +=         Gallery.photos[index].countComments;
    html += "   </div>";
    html += "   <div class='clear'>";
    html += "   </div>";
    html += "</div>";
    html += "   <div id='photo-more-info-corner'>";
    html += "   </div>";
    html += "</div>";
    $('body').append(html);

    ofset.left = ofset.left + 70;
    ofset.top = ofset.top - $("#photo-more-info-wrapper").height() + 50;
    $("#photo-more-info-wrapper").css('left',ofset.left + "px").css('top',ofset.top + "px").css('visibility','visible');
}

function unClickedPhotos(){
    $("#photo-more-info-wrapper").remove();
    $(".photo-wrapper").each(function(index,element){
        var jObj = $(element);
        if (jObj.hasClass("checked")) {
            jObj.children(".photo-text").css('visibility','visible');
            jObj.children(".cover-photo").remove();
            jObj.removeClass("checked");
        }
    });
}