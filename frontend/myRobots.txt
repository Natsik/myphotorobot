������ 83
���������� ��������� � ��� ����� � ������
��� ����������� �������� �������� json
����� POST
����� ���� ���
{
    "id":"123", //���� ������
    "socials":[
        {
            "iconUrl":"vk",          // ������� �������� ���������� ���� (vk,fb,insta)
            "login":"123",
            "password":"123",
            "using":"true"
        },
        {
            "iconUrl":"fb",          // ������� �������� ���������� ���� (vk,fb,insta)
            "login":"123",
            "password":"123",
            "using":"true"
        },
        {
            "iconUrl":"insta",          // ������� �������� ���������� ���� (vk,fb,insta)
            "login":"123",
            "password":"123",
            "using":"false"
        },
    ]
}

������ 104
����� ������� �������� � �� �������� ��� ������
�������� ������� POST ������ { id:123 }
������ ����������� ������ json
[
  {
    "iconUrl":"vk",
    "login":"avakado",
    "password":"avakado123",
    "using": "false",
    "info":"������� ����� � ������ ������������ �������� ��� ������� � ���������, ���������, ��� ������ ������� ����� ������ � �������� ���������� � ��������� ������. ID - ������� ����� ����� � URL �������� ������� �������"
  },
  {
    "iconUrl":"fb",
    "login":"mango",
    "password":"mango123123",
    "using": "true",
    "info":"������� ����� � ������ �������� ��� ������� � Instagram"
  },
  {
    "iconUrl":"insta",
    "login":"watermelon",
    "password":"123watermelon123",
    "using": "true",
    "info":"������� ����� � ������ ������������ �������� ��� ������� � Facebook, ���������, ��� ������ ������� ����� ������ � �������� ���������� � ��������� ������. ID - ������� ����� ����� � URL �������� ������� �������"
  }
]

������ 235
����� ������ ������� � �� ����������
������ ���������� ������ json
[
  {
    "id":"1",
    "imageUrl":"img/default.jpg",
    "address":"�����-���������, ��. ������� �.6 �.1",
    "status":"ok",
    "socials":[
      {
        "iconUrl":"vk",
        "hrefUrl":"vk.com"
      },
      {
        "iconUrl":"fb",
        "hrefUrl":""            // ��������� ����� ������� � ���, ��� ������ �������� ���������
      },
      {
        "iconUrl":"insta",
        "hrefUrl":""            // ��������� ����� ������� � ���, ��� ������ �������� ���������
      }
    ]
  },
  {
    "id":"2",
    "imageUrl":"img/default.jpg",
    "address":"�����-���������, ��. ������� �.5 �.1",
    "status":"ok",
    "socials":[
      {
        "iconUrl":"vk",
        "hrefUrl":"vk.com"
      },
      {
        "iconUrl":"fb",
        "hrefUrl":""
      },
      {
        "iconUrl":"insta",
        "hrefUrl":""
      }
    ]
  },
  {
    "id":"3",
    "imageUrl":"img/default.jpg",
    "address":"�����-���������, ��. ������� �.6 �.1",
    "status":"ok",
    "socials":[
      {
        "iconUrl":"vk",
        "hrefUrl":"vk.com"
      },
      {
        "iconUrl":"fb",
        "hrefUrl":"facebook.com"
      },
      {
        "iconUrl":"insta",
        "hrefUrl":"vk.com"
      }
    ]
  }
]
